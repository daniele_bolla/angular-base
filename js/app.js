var app = angular.module('backand', ['ngRoute'])

.config(['$routeProvider', '$httpProvider', '$locationProvider', '$sceDelegateProvider',
  function($routeProvider, $httpProvider, $locationProvider, $sceDelegateProvider) {
    $locationProvider.html5Mode(true);
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    $routeProvider
      .when('/', {
        templateUrl: 'view/home.htm',
        controller: 'auth'
      })
    ;
		
		$sceDelegateProvider.resourceUrlWhitelist([
			// Allow same origin resource loads.
			'self',
			// Allow loading from our assets domain.  Notice the difference between * and **.
			'https://cdnjs.cloudflare.com/**'
  	]);
}])


.controller('global', function($scope) {
	$scope.site = {
		name: 'Backand',
		head: {
			js: [
				{
					name: 'bootstrap',
					src: 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js'
				}
			],
			
			css: [
				{
					name: 'bootstrap',
					src: 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css'
				}, {
					name: 'fontawesome',
					src: 'http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'
				}
			]
			
		},
		nav: {
			links: [
				{href:'contacts', text:'Contacts', icon:'user'}
			]
		}
	};
	
	var js = $scope.site.head.js;
	for (i = 0; i < js.length; i++) {
		var script = document.createElement('script');
		script.src = js[i].src;
		document.getElementsByTagName('head')[0].appendChild(script);
	}
	
	var css = $scope.site.head.css;
	for (i = 0; i < js.length; i++) {
		link = document.createElement('link');
		link.href = css[i].src;
		link.rel = 'stylesheet';
		document.getElementsByTagName('head')[0].appendChild(link);
	}
	
})

.directive('jheader', function() {
	return {
		templateUrl: 'view/jheader.htm',
	};
})
;